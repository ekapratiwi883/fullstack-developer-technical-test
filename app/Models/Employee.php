<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = [
        'position_id', 'nama', 'NIP', 'tanggal_lahir',
        'alamat', 'no_telepon', 'agama', 'status', 'file_ktp'
    ];
    protected $primaryKey='id';
    protected $table = 'employees';

    public function position() {
        return $this->belongsTo(Position::class, 'position_id');
    }
}
