<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama_jabatan', 'departemen'
    ];

    public function position() {
        return $this->hasMany(Employee::class, 'position_id');
    }
}
