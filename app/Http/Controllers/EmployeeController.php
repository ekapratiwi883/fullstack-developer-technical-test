<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use Illuminate\Http\Request;
use Storage;
use Illuminate\Support\Facades\File;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $position = Position::all();
        $employee = Employee::with('position')->get();
        return view('employee', compact('employee', 'position'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'nama' => 'required',
            'NIP' => 'required|unique:employees',
            'tanggal_lahir' => 'required',
            'alamat' => 'required',
            'no_telepon' => 'required|numeric|min:12',
            'agama' => 'required',
            'status' => 'required',
            'file_ktp' => 'required|mimes: jpg,jpeg,png'
        ]);

        $file_name = $request->file('file_ktp')->getClientOriginalName();
       $request->file('file_ktp')->move('upload/', $file_name);
    // $file= $request->file('file_ktp');
    //     $file->move('upload', $file->getClientOriginalName());
    //     $file_name=$file->getClientOriginalName();
        

       Employee::create([
            'position_id' => $request->position_id,
            'nama' => $request->nama,
            'NIP' => $request->NIP,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
            'no_telepon' => $request->no_telepon,
            'agama' => $request->agama,
            'status' => $request->status,
            'file_ktp' => $file_name,
        ]);
        
        return redirect('employee')->with('success', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        return view('employeeDetail', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::find($id);
        $employee->position_id = $request->position_id;
        $employee->nama = $request->nama;
        $employee->NIP = $request->NIP;
        $employee->tanggal_lahir = $request->tanggal_lahir;
        $employee->alamat = $request->alamat;
        $employee->no_telepon = $request->no_telepon;
        $employee->agama = $request->agama;
        $employee->status = $request->status;

        if ($request->hasFile('file_ktp')) 
        {
            Storage::delete($employee->file_ktp);
            $file_name = $request->file('file_ktp')->getClientOriginalName();
            $request->file('file_ktp')->move('upload/', $file_name);
            $employee->file_ktp = $file_name;
        }
        
        
        $employee->save(); 
        return redirect('employee')->with('success', 'Data Berhasil Diubah');

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        return redirect('employee')->with('success', 'Data Berhasil Dihapus');
    }
}
