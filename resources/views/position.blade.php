@extends('layout.app')

@section('css')
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Jabatan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Jabatan</li>
    </ol>
</div>
@endsection
@section('content')
<div class="card sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
        <button type="button" class="btn btn-primary add-btn" data-toggle="modal" data-target="#addpositionModal">
          Add Position
        </button>
    </div>
    <div class="card-body">
        <div class="table-responsive p-3">
            <table class="table align-items-center table-flush" id="dataTable">
              <thead class="thead-light">
                <tr>
                  <th>No.</th>
                  <th>Nama Jabatan</th>
                  <th>Departemen</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($positions as $item)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$item->nama_jabatan}}</td>
                  <td>{{$item->departemen}}</td>
                  <td>
                    <a href="" class="btn btn-warning" data-toggle="modal" data-target="#updateModal{{$item->id}}"><i class="fa fa-edit"></i></a>
                    {{-- <a href="{{ route('position.destroy', $item->id) }}" class="btn btn-warning"><i class="fa fa-pencil-square"></i></a> --}}
                   
                    <form class="d-inline" action="{{ route('position.destroy', $item->id) }}" method="POST">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-danger" ><i class="fa fa-trash"></i></button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="addpositionModal" tabindex="-1" aria-labelledby="addpositionModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addpositionModalLabel">Add Position</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="{{route('position.store')}}" method="POST">
        @csrf
          <div class="row">
            <div class="col-lg-12 mb-2">
              <label for="nama">Nama Jabatan</label>
              <input type="text" name="nama_jabatan" id="nama_jabatan" class="form-control @error('nama_jabatan') is-invalid @enderror" required>
              @error('nama_jabatan')
                  <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
            <div class="col-lg-12 mb-2">
              <label for="nama">Departemen</label>
              <input type="text" name="departemen" id="departemen" class="form-control" required>
            </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-save">Add position</button>
      </div>
    </form>
    </div>
  </div>
</div>

@foreach ($positions as $data)
<div class="modal fade" id="updateModal{{ $data->id }}" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateModalLabel">Edit Position</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="{{ route('position.update', $data->id) }}" method="POST">
        @csrf
        @method('PUT')
          <div class="row">
            <div class="col-lg-12 mb-2">
              <label for="nama">Nama Jabatan</label>
              <input type="text" name="nama_jabatan" id="nama_jabatan" value="{{$data->nama_jabatan}}" class="form-control" required>
            </div>
            <div class="col-lg-12 mb-2">
              <label for="nama">Departemen</label>
              <input type="text" name="departemen" id="departemen" value="{{$data->departemen}}" class="form-control" required>
            </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-save">Add position</button>
      </div>
    </form>
    </div>
  </div>
</div>
@endforeach

@include('sweetalert::alert')
@endsection

@push('js')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

{{-- <script>
    $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('position.index') }}",
        columns: [
            { data: 'DT_RowIndex', orderable: false, searchable: false, className: "text-center"},
            { data: 'nama_jabatan' },
            { data: 'departemen' }
        ]
    });

    $.ajaxSetup({
      headers:{
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    })

    $('body').on('click', '.add-btn', function(e){
        e.preventDefault();
        $('#addpositionModal').modal('show');

        $('.btn-save').click(function(){
          

          $.ajax({
            url: 'position',
            type: 'POST',
            data:{
              nama_jabatan: $('#nama_jabatan').val(),
              departemen: $('#departemen').val()
            },
            dataType: 'JSON',
            success: function(response){
             
                // alert('Berhasil');
                $('#nama_jabatan').val('');
                $('#departemen').val('');
                $('#addpositionModal').modal('hide');
                $('#dataTable').DataTable().ajax.reload()
            }
            
          });

          
        });
    });

</script> --}}
@endpush
