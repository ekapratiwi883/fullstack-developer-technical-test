<!-- Modal -->
<div class="modal fade" id="addemployeeModal" tabindex="-1" aria-labelledby="addemployeeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addemployeeModalLabel">Add Position</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <form action="{{route('employee.store')}}" method="POST" enctype="multipart/form-data">
          @csrf
            <div class="row">
              <div class="col-lg-12 mb-2">
                  <label for="nama">Jabatan</label>
                  <select class="default-select wide form-control" name="position_id">
                      <option disabled selected>--pilih jabatan--</option>
                  @foreach($position as $item)
                      <option value="{{$item->id}}">{{$item->nama_jabatan}}</option>
                  @endforeach
              </select>
                </div>
              <div class="col-lg-12 mb-2">
                <label for="nama">Nama</label>
                <input type="text" name="nama" id="nama" class="form-control" required>
                @error('nama')
                  {{$message}}
                @enderror
              </div>
              
              <div class="col-lg-12 mb-2">
                <label for="NIP">NIP</label>
                <input type="text" name="NIP" id="NIP" class="form-control">
                @error('NIP') 
                  <span class="text-danger">{{$message}}</span>
                @enderror
              </div>
              <div class="col-lg-12 mb-2">
                  <label for="tanggal_lahir">Tanggal Lahir</label>
                  <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" required>
                </div>
                <div class="col-lg-12 mb-2">
                  <label for="alamat">Alamat</label>
                  <input type="text" name="alamat" id="alamat" class="form-control" required>
                </div>
                <div class="col-lg-12 mb-2">
                  <label for="no_telepon">Nomer Telepon</label>
                  <input type="text" name="no_telepon" id="no_telepon" class="form-control" required>
                </div>
                <div class="col-lg-12 mb-2">
                  <label for="agama">Agama</label> <br>
                  <input type="radio" value="Islam" id="islam" name="agama">
                  <label for="islam">Islam</label>&nbsp
                  <input type="radio" value="Kristen" id="kristen" name="agama">
                  <label for="kristen">Kristen</label>&nbsp
                  <input type="radio" value="Hindu" id="hindu" name="agama">
                  <label for="hindu">Hindu</label>&nbsp
                  <input type="radio" value="Buddha" id="buddha" name="agama">
                  <label for="buddha">Buddha</label>&nbsp
                  <input type="radio" value="Katolik" id="katolik" name="agama">
                  <label for="katolik">Katolik</label>&nbsp
                  <input type="radio" value="Konghuchu" id="Konghuchu" name="agama">
                  <label for="Konghuchu">Kong hu Chu</label>
                </div>
                <div class="col-lg-12 mb-2">
                  <label for="status">status</label>
                  <select class="default-select wide form-control" name="status">
                      <option disabled selected>--pilih status--</option>
                      <option value="1">Active</option>
                      <option value="0">Nonactive</option>
                  </select>
                </div>
                <div class="col-lg-12 mb-2">
                  <label for="file_ktp">Foto KTP</label>
                  <input type="file" name="file_ktp" id="file_ktp" class="form-control" required>
                </div>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary btn-save">Add Employee</button>
        </div>
      </form>
      </div>
    </div>
  </div>