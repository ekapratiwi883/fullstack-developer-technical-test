@extends('layout.app')

@section('css')
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Jabatan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Jabatan</li>
    </ol>
</div>
@endsection
@section('content')
<div class="card sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
        <button type="button" class="btn btn-primary add-btn" data-toggle="modal" data-target="#addemployeeModal">
          Add Position
        </button>
    </div>
    @if (count($errors)>0)
        <div class="alert alert-danger">
          <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
          </ul>
        </div>
    @endif
    <div class="card-body">
        <div class="table-responsive p-3">
            <table class="table align-items-center table-flush" id="dataTable">
              <thead class="thead-light">
                <tr>
                  <th>No.</th>
                  <th>Nama Jabatan</th>
                  <th>Departemen</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach ($employee as $item)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$item->nama}}</td>
                  <td>{{$item->position->nama_jabatan}}</td>
                  <td>
                    @if($item->status == 1)
                    <button class="btn btn-outline-success btn-sm" style="width: 100%" disabled>Active</button>
                  @else
                  <button class="btn btn-outline-danger btn-sm" style="width: 100%" disabled>Nonactive</button>
                  @endif
                
                  </td>
                  <td>
                    <a href="{{ route('employee.show', $item->id) }}" class="btn btn-light" ><i class="fa fa-eye"></i></a>
                    <a href="" class="btn btn-warning" data-toggle="modal" data-target="#updateModal{{$item->id}}"><i class="fa fa-edit"></i></a>
                    {{-- <a href="{{ route('position.destroy', $item->id) }}" class="btn btn-warning"><i class="fa fa-pencil-square"></i></a> --}}
                   
                    <form class="d-inline" action="{{ route('employee.destroy', $item->id) }}" method="POST">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-danger" ><i class="fa fa-trash"></i></button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
    </div>
</div>






@include('sweetalert::alert')
@include('employeeAdd')
@include('employeeEdit')

@endsection

@push('js')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

@endpush
