@extends('layout.app')

@section('css')
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Jabatan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Jabatan</li>
    </ol>
</div>
@endsection
@section('content')
<div class="card sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
        <a href="{{ route('employee.index') }}" class="btn btn-success">Back</a>
    </div>
    <div class="card-body">
        <div class="table-responsive p-3">
            <table class="table table-bordered align-items-center table-flush" id="dataTable">
                @php
                    $i = 1;
                @endphp
                <tr>
                  <th width="25%">No </th>
                  <td>:&nbsp{{$i++}}</td>
                </tr>
                <tr>
                    <th>Nama </th>
                    <td>:&nbsp{{$employee->nama}}</td>
                </tr>
                <tr>
                    <th>NIP </th>
                    <td>:&nbsp{{$employee->NIP}}</td>
                </tr> 
                <tr>
                    <th>Jabatan </th>
                    <td>:&nbsp{{$employee->position->nama_jabatan}}</td>
                </tr>
                <tr>
                    <th>Departemen </th>
                    <td>:&nbsp{{$employee->position->departemen}}</td>
                </tr>
                <tr>
                    <th>Tanggal Lahir </th>
                    <td>:&nbsp{{$employee->tanggal_lahir}}</td>
                </tr> 
                <tr>
                    <th>Alamat </th>
                    <td>:&nbsp{{$employee->alamat}}</td>
                </tr> 
                <tr>
                    <th>No Telepon </th>
                    <td>:&nbsp{{$employee->no_telepon}}</td>
                </tr> 
                <tr>
                    <th>Agama</th>
                    <td>:&nbsp{{$employee->agama}}</td>
                </tr> 
                <tr>
                    <th>Status</th>
                    <td>
                        @if($employee->status == 1)
                        <button class="btn btn-outline-success btn-sm" style="width: 25%" disabled>Active</button>
                      @else
                      <button class="btn btn-outline-danger btn-sm" style="width: 25%" disabled>Nonactive</button>
                      @endif
                    </td>
                </tr> 
                <tr>
                    <th>Foto KTP</th>
                    <td><img src="{{asset('upload/'.$employee->file_ktp) }}" width="120px"></td>
                </tr>
            
            </table>
          </div>
    </div>
</div>



@endsection

@push('js')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

@endpush
