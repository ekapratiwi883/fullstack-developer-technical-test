@foreach ($employee as $data)
<div class="modal fade" id="updateModal{{ $data->id }}" tabindex="-1" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateModalLabel">Edit Position</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="{{ route('employee.update', $data->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-lg-12 form-group mb-2">
                <label for="nama">Jabatan</label>
                <select class="default-select wide form-control" name="position_id">
                    <option value="{{$data->position_id}}">{{$data->position->nama_jabatan}}</option>
                @foreach($position as $item)
                    <option value="{{$item->id}}">{{$item->nama_jabatan}}</option>
                @endforeach
            </select>
              </div>
            <div class="form-group col-lg-12 mb-2">
              <label for="nama">Nama</label>
              <input type="text" name="nama" id="nama" value="{{$data->nama}}" class="form-control" required>
              
            </div>
            
            <div class="form-group col-lg-12 mb-2">
              <label for="NIP">NIP</label>
              <input type="text" name="NIP" id="NIP" value="{{$data->NIP}}" class="form-control @error('NIP') is-invalid @enderror">
              @error('NIP') 
                <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
            <div class="form-group col-lg-12 mb-2">
                <label for="tanggal_lahir">Tanggal Lahir</label>
                <input type="date" name="tanggal_lahir" value="{{$data->tanggal_lahir}}" id="tanggal_lahir" class="form-control" required>
              </div>
              <div class="form-group col-lg-12 mb-2">
                <label for="alamat">Alamat</label>
                <input type="text" name="alamat" id="alamat" value="{{$data->alamat}}" class="form-control" required>
              </div>
              <div class="form-group col-lg-12 mb-2">
                <label for="no_telepon">Nomer Telepon</label>
                <input type="text" name="no_telepon" id="no_telepon" value="{{$data->no_telepon}}" class="form-control" required>
              </div>
              <div class="form-group col-lg-12 mb-2">
                <label for="agama">Agama</label> <br>
                <input type="radio" value="Islam" id="islam" name="agama" {{$data->agama == 'Islam' ? 'checked' : ''}}>
                <label for="islam">Islam</label>&nbsp
                <input type="radio" value="Kristen" id="kristen" name="agama" {{$data->agama == 'Kristen' ? 'checked' : ''}}>
                <label for="kristen">Kristen</label>&nbsp
                <input type="radio" value="Hindu" id="hindu" name="agama" {{$data->agama == 'Hindu' ? 'checked' : ''}}>
                <label for="hindu">Hindu</label>&nbsp
                <input type="radio" value="Buddha" id="buddha" name="agama" {{$data->agama == 'Buddha' ? 'checked' : ''}}>
                <label for="buddha">Buddha</label>&nbsp
                <input type="radio" value="Katolik" id="katolik" name="agama" {{$data->agama == 'Katolik' ? 'checked' : ''}}>
                <label for="katolik">Katolik</label>&nbsp
                <input type="radio" value="Konghuchu" id="Konghuchu" name="agama" {{$data->agama == 'Konghuchu' ? 'checked' : ''}}>
                <label for="Konghuchu">Kong hu Chu</label>
              </div>
              <div class="form-group col-lg-12 mb-2">
                <label for="status">status</label>
                <select class="default-select wide form-control" name="status">
                    @if ($data->status == 1)
                    <option value="1" selected>Active</option>
                    <option value="0">Nonactive</option>
                    @else
                    <option value="1">Active</option>
                    <option value="0" selected>Nonactive</option>
                    @endif
                    
                    
                </select>
              </div>
              <div class="form-group col-lg-12 mb-2">
                <label for="file_ktp">Foto KTP</label>
                <input type="file" name="file_ktp" id="file_ktp" class="form-control">
                <img src="{{ asset('upload/'.$data->file_ktp) }}" width="120px">
              </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-save">Add position</button>
      </div>
    </form>
    </div>
  </div>
</div>
@endforeach